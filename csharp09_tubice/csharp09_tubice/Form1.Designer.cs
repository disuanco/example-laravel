﻿namespace csharp09_tubice
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CB_choose = new System.Windows.Forms.ComboBox();
            this.Nud_1 = new System.Windows.Forms.NumericUpDown();
            this.cb_industrial = new System.Windows.Forms.ComboBox();
            this.btn_compute = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbl_type = new System.Windows.Forms.Label();
            this.lbl_rate = new System.Windows.Forms.Label();
            this.lbl_total = new System.Windows.Forms.Label();
            this.btn_next = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Nud_1)).BeginInit();
            this.SuspendLayout();
            // 
            // CB_choose
            // 
            this.CB_choose.FormattingEnabled = true;
            this.CB_choose.Items.AddRange(new object[] {
            "Residentail",
            "Commercial",
            "Industrial"});
            this.CB_choose.Location = new System.Drawing.Point(194, 38);
            this.CB_choose.Name = "CB_choose";
            this.CB_choose.Size = new System.Drawing.Size(198, 21);
            this.CB_choose.TabIndex = 0;
            this.CB_choose.SelectedIndexChanged += new System.EventHandler(this.CB_choose_SelectedIndexChanged);
            // 
            // Nud_1
            // 
            this.Nud_1.DecimalPlaces = 4;
            this.Nud_1.Location = new System.Drawing.Point(272, 140);
            this.Nud_1.Name = "Nud_1";
            this.Nud_1.Size = new System.Drawing.Size(120, 20);
            this.Nud_1.TabIndex = 2;
            this.Nud_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cb_industrial
            // 
            this.cb_industrial.FormattingEnabled = true;
            this.cb_industrial.Items.AddRange(new object[] {
            "Peak Hours",
            "Off-Peak Hours"});
            this.cb_industrial.Location = new System.Drawing.Point(225, 77);
            this.cb_industrial.Name = "cb_industrial";
            this.cb_industrial.Size = new System.Drawing.Size(152, 21);
            this.cb_industrial.TabIndex = 4;
            // 
            // btn_compute
            // 
            this.btn_compute.Location = new System.Drawing.Point(284, 256);
            this.btn_compute.Name = "btn_compute";
            this.btn_compute.Size = new System.Drawing.Size(108, 37);
            this.btn_compute.TabIndex = 5;
            this.btn_compute.Text = "Compute";
            this.btn_compute.UseVisualStyleBackColor = true;
            this.btn_compute.Click += new System.EventHandler(this.btn_compute_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(225, 327);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(167, 36);
            this.textBox1.TabIndex = 6;
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Location = new System.Drawing.Point(57, 46);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(83, 13);
            this.lbl_type.TabIndex = 7;
            this.lbl_type.Text = "Type of Building";
            // 
            // lbl_rate
            // 
            this.lbl_rate.AutoSize = true;
            this.lbl_rate.Location = new System.Drawing.Point(57, 147);
            this.lbl_rate.Name = "lbl_rate";
            this.lbl_rate.Size = new System.Drawing.Size(82, 13);
            this.lbl_rate.TabIndex = 8;
            this.lbl_rate.Text = "Rate of Usuage";
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Location = new System.Drawing.Point(95, 350);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(31, 13);
            this.lbl_total.TabIndex = 9;
            this.lbl_total.Text = "Total";
            // 
            // btn_next
            // 
            this.btn_next.Location = new System.Drawing.Point(284, 387);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(108, 37);
            this.btn_next.TabIndex = 10;
            this.btn_next.Text = "Next Costumer";
            this.btn_next.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(491, 446);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.lbl_total);
            this.Controls.Add(this.lbl_rate);
            this.Controls.Add(this.lbl_type);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_compute);
            this.Controls.Add(this.cb_industrial);
            this.Controls.Add(this.Nud_1);
            this.Controls.Add(this.CB_choose);
            this.Name = "Form1";
            this.Text = "Simple Power Billing";
            ((System.ComponentModel.ISupportInitialize)(this.Nud_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CB_choose;
        private System.Windows.Forms.NumericUpDown Nud_1;
        private System.Windows.Forms.ComboBox cb_industrial;
        private System.Windows.Forms.Button btn_compute;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.Label lbl_rate;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Button btn_next;

    }
}

